/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	v1 "my.demo.com/lazyapps/api/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
)

// LazyAppsReconciler reconciles a LazyApps object
type LazyAppsReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=webapp.my.demo.com,resources=lazyapps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=webapp.my.demo.com,resources=lazyapps/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=webapp.my.demo.com,resources=lazyapps/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=create
//+kubebuilder:rbac:groups="",resources=services,verbs=create
//+kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=create

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the LazyApps object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.12.2/pkg/reconcile
func (r *LazyAppsReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	lazyApp := &v1.LazyApps{}
	err := r.Get(ctx, req.NamespacedName, lazyApp)
	if err != nil {
		fmt.Println("error ", err.Error())
		return ctrl.Result{
			Requeue: false,
		}, nil
	}
	labels := map[string]string{}
	labels["app"] = req.Name

	deployment := newDeployment(*lazyApp, labels)
	service := newService(*lazyApp, labels)
	ingress := newIngress(*lazyApp, labels)

	err = r.Create(ctx, &deployment, &client.CreateOptions{})
	if err != nil {
		fmt.Println("error ", err.Error())
		return ctrl.Result{
			Requeue: false,
		}, nil
	}
	err = r.Create(ctx, &service, &client.CreateOptions{})
	if err != nil {
		fmt.Println("error ", err.Error())
		return ctrl.Result{
			Requeue: false,
		}, nil
	}
	err = r.Create(ctx, &ingress, &client.CreateOptions{})
	if err != nil {
		fmt.Println("error ", err.Error())
		return ctrl.Result{
			Requeue: false,
		}, nil
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *LazyAppsReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1.LazyApps{}).
		Complete(r)
}

func newIngress(lazyApp v1.LazyApps, labels map[string]string) networkingv1.Ingress {
	className := "nginx"
	pathType := networkingv1.PathTypePrefix
	ingress := networkingv1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      lazyApp.Name,
			Namespace: lazyApp.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				{
					APIVersion: lazyApp.APIVersion,
					Kind:       lazyApp.Kind,
					Name:       lazyApp.Name,
					UID:        lazyApp.UID,
				},
			},
		},
		Spec: networkingv1.IngressSpec{
			IngressClassName: &className,
			Rules: []networkingv1.IngressRule{
				{
					Host: lazyApp.Spec.URL,
					IngressRuleValue: networkingv1.IngressRuleValue{
						HTTP: &networkingv1.HTTPIngressRuleValue{
							Paths: []networkingv1.HTTPIngressPath{
								{
									Path:     "/",
									PathType: &pathType,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: lazyApp.Name,
											Port: networkingv1.ServiceBackendPort{
												Number: lazyApp.Spec.Port,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	return ingress
}
func newService(lazyApp v1.LazyApps, labels map[string]string) corev1.Service {
	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      lazyApp.Name,
			Namespace: lazyApp.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				{
					APIVersion: lazyApp.APIVersion,
					Kind:       lazyApp.Kind,
					Name:       lazyApp.Name,
					UID:        lazyApp.UID,
				},
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{
					Name:     "tcp",
					Protocol: corev1.ProtocolTCP,
					Port:     lazyApp.Spec.Port,
					TargetPort: intstr.IntOrString{
						IntVal: lazyApp.Spec.Port,
					},
				},
			},
			Selector: labels,
			Type:     corev1.ServiceTypeClusterIP,
		},
	}
	return service
}
func newDeployment(lazyApp v1.LazyApps, labels map[string]string) appsv1.Deployment {

	one := int32(1)
	deployment := appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      lazyApp.Name,
			Namespace: lazyApp.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				{
					APIVersion: lazyApp.APIVersion,
					Kind:       lazyApp.Kind,
					Name:       lazyApp.Name,
					UID:        lazyApp.UID,
				},
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &one,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  lazyApp.Name,
							Image: lazyApp.Spec.Image,
							Ports: []corev1.ContainerPort{
								{
									Name:          "tcp",
									Protocol:      corev1.ProtocolTCP,
									ContainerPort: lazyApp.Spec.Port,
								},
							},
						},
					},
				},
			},
		},
	}
	return deployment
}
